<?php include_once('./common/header.php') ?>
<head>
<style>


.item{
top: 70%;
transform: translateY(-50%);
-webkit-transform-style: preserve-3d;
-moz-transform-style: preserve-3d;
transform-style: preserve-3d;

}
.rect {
  width: 100%;
  height: 300px;
  border: solid 0.5px #707070;
  background-color: #343144;
}
.pro
{
  
  color:#f89e20;
  font-size:31px;
}
.col-centered{
    float: none;
    margin: 0 auto;
}
.carousel-caption
{
  position:absolute;right:15%;
  bottom:20px;
  left:15%;
  z-index:10;
  padding-top:10px;
  padding-bottom:193px;
  text-align:center
}
.gradient-button {
  position: absolute;
  z-index: 1;
  display: block;
  top: calc(54% - 2.5rem - 5px);
  left: calc(70% - 6rem - 5px);
  height: 3rem;
  width: 10rem;
  margin: auto;
  background: transparent ;
    border-image-source: linear-gradient(to right, #f7941d, #f18f21 13%, #e0832f 31%, #c47045 51%, #9d5464 74%, #6c318b 97%, #662d91);
  border-image-slice: 1;
  transition: transform .25s;
  letter-spacing: .2rem;
  font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  font-size: 0.75rem;
  font-weight: 300;
  text-align: center;
  text-decoration: none;
  text-transform: uppercase;
  color: #333;
}

.gradient-button:active {
  transform: scale(0.96);
}
.gradient-button:active::before {
  opacity: 1;
}
.gradient-button::before {
  content: "";
  position: absolute;
  top: -5px;
  left: -5px;
  width: 100%;
  height: 100%;
  margin: auto;
  
  opacity: 0;
  pointer-events: none;
  border-image-slice: 1;
  z-index: 0;
  border-image-source: linear-gradient(to bottom left, #f9d081 20%, #e3023e 40%, #318087 70%);
  transition: opacity .5s;
}
.gradient-button:hover::after {
  opacity: 1;
}

.front-Page {
  width: 683px;
  height: 2675.5px;
  background-color: #ffffff;
}
.card
{
       position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #f7f7f7;
    background-clip: border-box;
    border: 0.1px solid rgba(0,0,0,.125);
    border-radius: .25rem;
}
.Subs{
  width: 100%;
  
  font-family: Montserrat;
  font-size: 10px;
  font-weight: 300;
  font-style: normal;
  font-stretch: normal;
  line-height: 0;
  letter-spacing: normal;
  text-align: left;
  color: #ffffff;
}
footer {
  
  color: #000000;
  font-size: 23px;
  padding: 32px;
}


</style>
</head>
<body>
  <div class="bd-example">
  <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="./img/7.png" class="d-block w-100" alt="...">
        <div class="container align-self-center">

        <div class="carousel-caption mx-auto;" style="color:#000000;">
          <h1 class="display-4" style="font-size:60px;font-weight: 400;"><b>SHEILD</b><b style="font-size:81px;font-weight: 400;">X</b></h1>
          <p class="lead" style="font-size:30px;font-weight: 100;">Bank-Grade Vaults To Secure Your Mnemonic Phrases</p>
          <br>
          <br>
          <div class="container col-xs-2" style="padding-left:190px;"> 
            <input style="width:40%; " id="email" type="text" class="form-control" name="email" placeholder="Email">&nbsp;
             <button type="button" class="gradient-button">Join The Queue</button>
          </div>
        </div>
      </div>
    </div>
      <div class="carousel-item">
        <img src="./img/7.png" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5>Second slide label</h5>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </div>
      </div>
      <div class="carousel-item">
        <img src="./img/7.png" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5>Third slide label</h5>
          <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>

 <div class="rect ">
      <p class="pro d-flex justify-content-center">
        <br>
        <br>
        
            A PRODUCT COMPATIBLE WITH <br>
             EVERY HARDWARE, SOFTWARE &<br>
               ONLINE WALLET.<br>    
 
</p>
</div>
<section style="height:400px">
    <div class="container col-md-12">
      <h1 style="padding-left: 330px;padding-top: 20px;"><b>Video
      </b>
    </h1>
    <br>
  </div>
  </section>
<div class="row text-center" style="height:600px;">
      <div class="col-sm-6">
        <div class="thumbnail">
          <p><strong>Robust Hardware Security</strong></p>
          <img src="./img/Group 109.png" alt="" width="50" height="50">
          
          <p>Current paper-based methods expose<br> recovery phrases encoding private keys.</p>
         
        </div>
        <br>
        <br>
        <br>
          <p><strong>Distributed Architechture</strong></p>
          <img src="./img/net.png" alt="" width="50" height="50">
          
          <p> No single point of failure wallet and <br>
             recovery phrases can  be distributed.</p>
             <br>
        <br>
        <br>
           <p><strong>Quantum-Proof</strong></p>
          <img src="./img/at.png" alt="" width="50" height="50">
          
          <p> Lorem ipsum dolor sit amet,<br> consectetur adipiscing elit. </p>
             <br>
        <br>
        <br>
         
        </div>
        
       
          
    
      <div class="col-sm-6">
        <div class="thumbnail">
          <p><strong>Isolated and Cold</strong></p>
          <img src="./img/Group 108.png" alt="" width="50" height="50">
          
          <p>Disconnected from the internet so that<br> there’s no chance of malware attacks.</p>
         
        </div>
        <br>
        <br>
        <br>
          <p><strong>Environment Proof</strong></p>
          <img src="./img/Group 107.png" alt="" width="50" height="50">
          
          <p> Premium quality ensures protection<br> from fire, water 

            </p>
         
        
        <br>
        <br>
        <br>
        <p><strong>Tamper Resistant</strong></p>
          <img src="./img/tamper.png" alt="" width="50" height="50">
          
          <p>Lorem ipsum dolor sit amet,<br> consectetur adipiscing elit. </p>
             <br>
        <br>
        <br>
       
      </div>
      
  </div>
  <br>
  <br>
  <div class="container col-xs-2" style="padding-left:150px;width:70%;background: #f7f7f7;">
    <div class="card  text-black">
     <div class="card-body">
          <h1 style="padding-left: 30px;padding-top: 20px;">Featured in  <img src="./img/cd.png" alt="Paris" width="145" height="82"></h1>
        </div>
      </div>
      </div>

      <br>
      <br>
      <div class="container col-md-12">
      <h1 style="padding-left: 330px;padding-top: 20px;"><b>Trusted By Experts
      </b>
    </h1>
    <br>
  </div>
      <div class="container text-center">
      <div class="row">
    <div class="col-sm-4">
      <div class="card" style="width: 18rem;">
  <div class="card-body">
    
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    
  </div>
</div>
    </div>
    <div class="col-sm-4">
      <div class="card" style="width: 18rem;">
  <div class="card-body">
    

    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    

  </div>
</div>
    </div>
    <div class="col-sm-4">
     <div class="card" style="width: 18rem;">
  <div class="card-body">
   

    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    

  </div>
</div>
    </div>
  </div>
</div>
<br>
<br>
 <div class="rect ">
      <p class="pro d-flex justify-content-center">
        <br>
      
        <div class="container col-xs-2" style="padding-left:190px;"> 
          <div class="Subs">
           <h1> Subscribe to our Newsletter.</h1><br>
            <h3>And don’t miss out on any updates from Cypherock.</h3>
          </div>
          <br>
          <div class="row">
            <div class="col-sm-4">
            <input style="width:100%; " id="email" type="text" class="form-control" name="email" placeholder="Email">
          </div>
          <div class="col-sm-md-4">
            &nbsp;
          </div>
          <div class="col-sm-md-4 ">
           <button class="btn-unfill nav-link "  type="submit"><div style="color:#ffffff;">Subscribe</button>
           </div>

        </div>

      </p>
    </div>
    <br>
    <br>

<footer class="text-center">
   <div class="row">
            <div class="col-sm-4">
            <img src="./img/foot.png" alt="Paris" width="150" height="12">
          </div>
          <div class="col-sm-md-4">
            <div class="row" style="font-size: 10px;padding-left: 100px;">
              <div class="col-sm-md-2">
                <b>Product</b>
                <ul>
                  <li>SheildX</li>
                  <li>Shop</li>
                  <li>Blog</li>
                </ul>
              </div>
              <div class="col-sm-md-2">
                <b>Help</b>
                <ul>

                  <li>Wiki</li>
                  <li>FAQ</li>
                  <li>Mail us </li>
                </ul>

              </div>
          </div>
        </div>
        &nbsp;
          <div class="col-sm-md-4" style="padding-left: 100px;">
           Follow us on<br>           
           <button type="button" class="btn btn-sm btn-fb" ><i class="fab fa-facebook-f"></i></button>
<!--Twitter-->
            <button type="button" class="btn btn-sm btn-tw" ><i class="fab fa-twitter"></i></button>
<!--Google +-->
            <button type="button" class="btn btn-sm btn-gplus" ><i class="fab fa-google-plus-g"></i></button>
<!--Linkedin-->
            <button type="button" class="btn  btn-li" ><i class="fab fa-linkedin-in"></i></button>
           </div>

        </div>
</footer>




  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>