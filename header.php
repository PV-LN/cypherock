<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Cypherock</title>
  
    <style>
        .d-flex.col{flex-direction: column;} 
        i.fab{color: #000000;font-size: 3.3rem;}
        nav .fnt
        {
        font-family: 'Nexa', cursive;
        font-weight:400;

        }
        
       
     

          .topnav a {
                float: center;
                display: block;
                background: black;
                text-align: center;
                text-decoration: none;
                font-size: 10px;
                
              }

              .topnav a:hover {
                
                color: #000000;
                }
           .navimg
             {
                
                height:100%;
                width:100%;
                position:fixed;
                opacity:2;
                 
                 background-color: #343144;

             }
           
            .home
            {
                
                    width: 100%;
                    height: 30px;
                    margin-top: 10px;
                    
                    font-family: 'Nexa', cursive;
                    font-weight:1000;
                    font-size:12px;
                    letter-spacing: 2px;
                    
            }
            .colo
            {
              color:#ffffff;
            }
            .colo:hover
            {
              color: #343144;
            }
            .home:hover
              {
  
                    border-radius: 5px;
                    width: 100%;
                    height: 30px;
                    margin-top: 10px;
                    
                    background-image: linear-gradient(to right, #f7941d 0%, #f8d62c 76%, #f9ed32);
                    
             }
              

.btn-unfill{
  transition: all 0.8s cubic-bezier(0.25, 1.01, 0.54, 1.29);display:inline-block;
    padding:5px 5px;background-color:#ffffff;border-radius:6px;text-transform:uppercase;
    font-weight:bold; background:transparent; border-color: #343144;}
.btn-unfill:hover, .btn-unfill:focus{ background-image: linear-gradient(to right, #fbb040 0%, #d6805b 37%, #92278f);}
.btn-unfill:hover {
  filter: url('#filter-goo-4');
}

.Front-Page {
  width: 683px;
  height: 2675.5px;
  background-color: #ffffff;
}


#myDiv{display:none;}
</style>
    <link rel="shortcut icon" href="favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#2b5797">
    <meta name="msapplication-config" content="favicons/browserconfig.xml">
    <meta name="theme-color" content="#2b5797">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/impress.css">
    <!-- Add icon library -->
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <script type="text/javascript"> function loadingDivHide() {document.getElementById("loader123").style.display = "none";document.getElementById("myDiv").style.display = "block";}</script>
</head>
<body onload="loadingDivHide()" ><div id="loader123"><div class="d-flex justify-content-center align-self-center"></div></div>
<div  id="myDiv" class="animate-bottom"></div>

    
    
        
    
   <nav class="navbar navbar-expand-lg navbar-light navimg  fnt" style="height:50px;">

     <a class="navbar-brand  animated wobble" href="#" style="width:120px;height:20px;margin-top: -10px;margin-left: 30px;">    
        <img src="./img/Group 125.png" class="img-fluid" style="width:100%;">
    </a>

 
   <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
    <div class="collapse navbar-collapse "  id="navbarNav">
    <ul class="navbar-nav" style="padding-left: 220px;" >
      <div class="row" >
        <div class="col">
      <li class="nav-item active">
        <a class="nav-link home  " href="#" ><div class="colo">HOME</div> <span class="sr-only">(current)</span></a>
      </li>
    </div>
    <div class="col">
      <li class="nav-item ">
        <a class="nav-link home " href="#" ><div class="colo">ABOUT</div></a> 
      </li>
    </div>
    <div class="col">
      <li class="nav-item ">
        <a class="nav-link home " href="#"><div class="colo">SHOP </div></a>
      </li>
    </div>
<div class="col">
      <li class="nav-item ">
        <a class="nav-link home " href="#" ><div class="colo">BLOG </div></a>
      </li>  
      </div> 
      <div class="col">
      <li class="nav-item ">
        <a class="nav-link home " href="#" ><div class="colo">WIKI</div> </a>
      </li> 
    </div>
    <div class="col">
      <li class="nav-item ">
        <button class="btn-unfill nav-link logfnt "  type="submit"><div style="color:#ffffff;">Subscribe</div></button>
        
      </li> 
    </div>

  </ul>
  </div>
    </nav>


  