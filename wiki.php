<?php include_once('./common/header.php') ?>
<head>
  <style>
  .material-icons.md-48 { font-size: 68px; }
  .fncol
  {

    color:#8c8c8d;
  }
  .hom
            {
                
                    width: 100%;
                    height: 30px;
                    margin-top: 10px;
                    border-radius: 20px;
                    background-image: linear-gradient(to right, #f7941d 0%, #f8d62c 76%, #f9ed32);
                    color: #343144;
                    font-weight:1000;
                    font-size:12px;
                    letter-spacing: 2px;
                    
            }

            
         
              
  </style>
</head>
<div class="container col-md-12" style="background:#f9f9f9">
      <h1  class="" style="padding-left: 360px;padding-top: 120px;font-size:30px;font-weight:400;"><b>How can We Help?

      </b>
    </h1>
    <br>
    <br>
   <div class="input-group md-form form-sm form-1 " style="padding-left:270px;width:890px;">
  
  <input class="form-control my-0 py-1"  type="text" placeholder="Search Cypherock wiki" aria-label="Search" style="border-radius:15px;height:50px;">
  <i class="fas fa-search text-black" style="color:#000000;" aria-hidden="true"></i>
</div>
<br>
<br>
</div>
    <br>
       <p class="container d-flex" style="padding-left:100px;"><b>
       Help Topics</p>
    <hr   style="width:40%;heigth:2px;border-color:#000000; margin-left: 8rem!important;padding-left:139px;">
 <br>
<br>


<div class="row text-center" style="height:600px;">
      <div class="col-sm-4" style="padding-left:135px;">
        <div class="thumbnail">
          <div class="row">
            <div class="col-md-4">
            <i class="material-icons md-48">person</i>
            </div>
            <div class="col-md-6" style="padding-right:10px;">
          <p style="color:#343144;">For Users</p>
          <p class="fncol" style="font-size:10px;"><i> Guide to get started, FAQ</i></p>
         </div>
         <br>
          
     </div>
        </div>
        <br>
        <hr   style="width:96%;heigth:0.7px;border-color:#000000;">

        <br>
        
        <div class="row">
            <div class="col-sm-4">
            <i class="material-icons md-48">code</i>
            </div>
            <div class="col-md-6" style="padding-right:10px;">
          <p style="color:#343144;">For Developers</p>
          <p class="fncol" style="font-size:10px;"><i> 
Intergration, Add Coins, Custom firmware </i></p>
         </div>
     </div>

     <hr   style="width:96%;heigth:0.7px;border-color:#000000;">
         
         <br>
       
          
        </div>
     
    
    
      <div class="col-sm-4" style="padding-left:135px;">
        <div class="thumbnail">
          <div class="row">
            <div class="col-md-4">
            <i class="material-icons md-48">lock</i>
            </div>
            <div class="col-md-6" style="padding-right:10px;">
          <p style="color:#343144;">Security</p>
          <p class="fncol" style="font-size:10px;"><i> 
                 Our Security Principles</i></p>
         </div>
     </div>
        </div>
        <br>
        <hr   style="width:96%;heigth:0.7px;border-color:#000000;">
        <br>
       
      
        <div class="row">
            <div class="col-sm-4">
            <i class="material-icons md-48">business</i>
            </div>
            <div class="col-md-6" style="padding-right:10px;">
          <p style="color:#343144;">For Business</p>
          <p class="fncol" style="font-size:10px;"><i> 
Affiliates, Resellers</i></p>
         </div>
     </div>
    <br>
     <hr   style="width:96%;heigth:0.7px;border-color:#000000;margin-top:4px;">
         
         <br>
        
         <div class="row" style="margin-left:-548px;">
            <div class="col-sm-4" style="padding-left:304px;">
            <i class="material-icons md-48">book</i>
            </div>
            <div class="col-md-6" style="padding-right:179px;">
          <p style="color:#343144;">For Knowledge</p>
          <p class="fncol" style="font-size:10px;"><i> 
           Glossary of cryptocurrency<br> and Cypherock Terms</i></p>
         </div>
     </div>

          
      </div>



      <div class="col-sm-4" style="margin-top:-97px;">
        <div class="thumbnail">
           <div class="card" style="width:14rem;background:#f2f2f2;border-radius:10px;">
  <div class="card-body">
   

    <p class="card-text" style="font-size:12px;">
    <b>Popular Articles</b><br>
    What are mnemonic phases? <br>
    Lorem ipsum dolor sit amet,<br> consectetur adipiscing elit?<br>
    Proin vitae sapien porta, <br>ornare ipsum vel, ultricies risus.<br>
    In scelerisque nulla ut tortor<br> malesuada, a malesuada purus ultrices?<br>
    Maecenas in fringilla velit, sit amet aliquet nibh. Nulla facilisi?</p>
    

  </div>
</div>
        </div>
        <br>
        
         <div class="card" style="width:13rem;background:#f2f2f2;border-radius:10px;">
  <div class="card-body">
   

    <p class="card-text" style="font-size:12px;">
    <b>Need Support?</b><br>
    Need Support?
       Can't find the answer you're looking for?<br>
        Don't worry we're here to help!<br>
        
        <button class=" hom" href="./about.php" >CONTACT SUPPORT</button> 
      
    </p>
    

  </div>
</div>
          
             <br>
        <br>
        <br>
          
        </div>

 

      
  </div>
