<?php include_once('./common/header.php') ?>

<head>
	<style>
.imgg {
  border-radius: 100%;
  color: #fafafa;
  display: flex;
  flex: 0 0 100px;
  height: 187px;
  justify-content: center;
  overflow: hidden;
  position: relative;
  width: 187px;
}
.img img {
  height: 100%;
}
.img__overlay {
  align-items: center;
  bottom: 0;
  display: flex;
  padding-top: 80px;
  left: 0;
  opacity: 0;
  position: absolute;
  right: 0;
  top: 0;
  transition: opacity 0.25s;
  z-index: 1;
}
.img__overlay:hover {
  opacity: 1;
}
/**
  * Theming
*/
span {
  bottom: 0;
  font-size: 24px;
  left: 0;
  line-height: 200px;
  position: absolute;
  right: 0;
  top: 0;
}
.imgg {
  -webkit-animation: fadeIn 0.5s;
          animation: fadeIn 0.5s;
  
  margin: 20px;
}
.img__overlay {
  background-color: #444444;
  
  color: #fafafa;
  font-size: 24px;
}
.img__overlay--green {
  background: linear-gradient(65deg, rgba(46,204,113,0.4), rgba(102,51,153,0.4) 60%);
  background-color: rgba(46,204,113,0.4);
}
.img__overlay--yellow {
  background: linear-gradient(65deg, #444444, #444444 60%);
  background-color: rgba(243,156,18,0.4);
}
.img__overlay--red {
 
  background-color: rgba(68,68,68,0.4);
}
@-webkit-keyframes fadeIn {
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
}
@keyframes fadeIn {
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
}


</style>
</head>
 <div class="container col-md-12">
      <h1 style="padding-left: 530px;padding-top: 20px;"><b>Our Vision 
      </b>
    </h1>
    <br>

    <p  class="container d-flex justify-content-center col-md-12">
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus aliquet iaculis dui. Nulla luctus ut dolor quis suscipit.
</p>
<p class="container d-flex justify-content-center col-md-12" style="padding-left:50px;">
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus aliquet iaculis dui. Nulla luctus ut dolor quis suscipit.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus aliquet iaculis dui. Nulla luctus ut dolor quis suscipit.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus aliquet iaculis dui. Nulla luctus ut dolor quis suscipit.
</p>
  </div>
  <br>
  <br>
  <div class="container col-md-12" style="background:#f9f9f9">
      <h1 style="padding-left: 360px;padding-top: 20px;font-size:60px;font-weight:600;"><b>FOUNDING TEAM
      </b>
    </h1>
    <br>
    <hr style="width:20%;heigth:2px;border-color: #f8d62c;">
    <p class="container d-flex justify-content-center col-md-12" style="padding-left:50px;"><b>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus aliquet iaculis dui. Nulla luctus ut dolor quis suscipit.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus aliquet iaculis dui.</b>
</p><br>
<br>
<div clas="container" style="padding-left:40px;">
 <div class="row">
    <div class="col-md-6">
      <br>
      <div class="cont" style="padding-left:263px;">
      	<a class="imgg" href="#">
         <div class="img__overlay img__overlay--red">
         <br>
         <br>
         <br>
         		<hr style="width:20%;heigth:2px;border-radius:20px;border-color:#ffffff;">
         	
         </div>
        <img src="./img/ceo.png"  class="img-circle person" alt="Random Name" width="187" height="187">
      </a>		
      
        <p class="text-center " style="font-size:20px;font-weight:300px;float:left;padding-left:13px;"><strong>ROHAN AGARWAL</strong></p>
        <p class="text-center figure-caption" style="font-size:20px;font-weight:300px;float:left;padding-left:35px;"><strong>Founder&CEO</strong></p><br>
   
      </div>

     </div>
    <div class="col-sm-4">
      <br>
        <div class="cont" style="padding-left:100px;">
      	<a class="imgg" href="#">
         <div class="img__overlay img__overlay--red">Hey!</div>
        <img src="./img/cto.png" class="img-circle person" alt="Random Name" width="187" height="187">
         </a>
     
 
      <p class="text-center" style="font-size:20px;font-weight:300px;padding-left:-10px;padding-right:70px;"><strong>VIPUL SAINI</strong></p>
      <p class="text-center figure-caption" style="font-size:20px;font-weight:300px;padding-right:65px;"><strong>Founder&CTO</strong></p><br>
      
   </div>
  </div>
</div>
</div>
</div>




<section style="min-height:63vh;padding-top:400px;">
<?php include_once('./common/footer.php') ?>
</section>